import Vue from 'vue'
import App from './App.vue'
import { VueSpinners } from '@saeris/vue-spinners'
import VueApexCharts from 'vue-apexcharts'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VCalendar from 'v-calendar';
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-default.css';

Vue.use(VueSpinners);
Vue.component('apexchart', VueApexCharts);
Vue.use(VueAxios, axios);
Vue.use(VCalendar);
Vue.use(VueToast, {
  position: 'top'
});

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
}).$mount('#app');
